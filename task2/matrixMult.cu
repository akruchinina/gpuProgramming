#include <iostream>
#include <math.h>
#include <time.h>

using namespace std;

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>

cudaError_t multMatrixWithCuda(int size, unsigned long long *a, unsigned long long *b, unsigned long long *c);
void multGpu(int size, unsigned long long *a, unsigned long long *b, unsigned long long *c);
void printGpuProperties();
void multCpu(int size, unsigned long long *a, unsigned long long *b, unsigned long long *c);

const int block_size = 32;

__global__ void multKernel(unsigned long long *a, unsigned long long *b, unsigned long long *c, int size)
{
	int bx = blockIdx.x, 
	int by = blockIdx.y;
	int tx = threadIdx.x;
	int ty = threadIdx.y;
	
	int aBegin = size * block_size * by;
	int aEnd = aBegin + size - 1;
	int bBegin = block_size * bx;
	int step = block_size * size;
	unsigned long long sum = 0;
		
	for(int i = aBegin, j = bBegin; i <= aEnd; i += step, j += step )
	{
		__shared__ unsigned long long as [block_size][block_size];
		__shared__ unsigned long long bs [block_size][block_size];
		as [ty][tx] = a [i + size * ty + tx];
		bs [ty][tx] = b [j + size * ty + tx];
		__syncthreads();
		for(int k = 0; k < block_size; k++)
		{
			sum += as [ty][k] * bs[k][tx];
		}
		__syncthreads();
	}
	c[size * block_size * by + block_size * bx + size * ty + tx] = sum;
}

void printMatrix(int matrixSize, unsigned long long *a)
{
	for (int i = 0; i < matrixSize; i++)
	{
		for (int j = 0; j < matrixSize; j++)
		{
			cout << a[i * matrixSize + j] << " ";
		}
		cout << endl;
	}
}

int main()
{
	int matrixSize;
    cout << "Enter matrix size: ";
    cin >> matrixSize;

	unsigned long long *matrixA = new unsigned long long [matrixSize * matrixSize];
	unsigned long long *matrixB = new unsigned long long [matrixSize * matrixSize];
	unsigned long long *matrixC = new unsigned long long [matrixSize * matrixSize];
	
	//��������� ������
	srand(time(NULL));	
    for (int i = 0; i < matrixSize; i++)
	{
		for (int j = 0; j < matrixSize; j++)
		{
			matrixA[i * matrixSize + j] = rand() % 10;
			matrixB[i * matrixSize + j] = rand() % 10;
			matrixC[i * matrixSize + j] = 0;
		}
	}

	//��������� ���������
	multGpu(matrixSize, matrixA, matrixB, matrixC);
	multCpu(matrixSize, matrixA, matrixB, matrixC);

	delete [] matrixA; 
    delete [] matrixB; 
    delete [] matrixC; 

    return 0;
}

void multCpu(int size, unsigned long long *a, unsigned long long *b, unsigned long long *c)
{
	unsigned long long result = 0;
	unsigned int start_time =  clock();
	
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
	 		for (int k = 0; k < size; k++)
			{
    			c[i * size + j] += a[i * size + k] * b[k * size + j];
			}
		}
	}

	unsigned int finish_time =  clock();	
	cout << "Time CPU is: " << finish_time - start_time << " ms" << endl;
	cout << "Matrix CPU is: " << endl;
	printMatrix(size, c);
}

void multGpu(int size, unsigned long long *a, unsigned long long *b, unsigned long long *c)
{ 
	cudaError_t cudaStatus = multMatrixWithCuda(size, a, b, c);
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "multMatrixWithCuda failed!");
        return;
    }

	cout << "Matrix GPU is: " << endl;
	printMatrix(size, c);
	cout << endl << endl;
  }

cudaError_t multMatrixWithCuda(int size, unsigned long long *a, unsigned long long *b, unsigned long long *c)
{
    int numBytes = size * size * sizeof(unsigned long long);
	unsigned long long *dev_a, *dev_b, *dev_c;	
	dim3 threads(block_size, block_size );
	dim3 blocks(size / threads.x, size / threads.y);

    cudaError_t cudaStatus;
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        goto Error;
    }

    //�������� ������
    cudaStatus = cudaMalloc((void**)&dev_a, numBytes);
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_b, numBytes);
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_c, numBytes);
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }


    //��������
	cudaStatus = cudaMemcpy(dev_a, a, numBytes, cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

	cudaStatus = cudaMemcpy(dev_b, b, numBytes, cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }
   
	size_t shm_size = 2 * block_size * block_size * sizeof(unsigned long long);
	
	//�������� �����
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	float time;
	cudaEventRecord(start, 0);

	multKernel<<<block_size, threads, shm_size>>>(dev_a, dev_b, dev_c, size);

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);

    //��������� ������
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "multKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        goto Error;
    }
    
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching multKernel!\n", cudaStatus);
        goto Error;
    }

    //�������� ���������
	cudaStatus = cudaMemcpy(c, dev_c, size * sizeof(unsigned long long), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

Error:
    cudaFree(dev_a);
    cudaFree(dev_b);
    cudaFree(dev_c);

	printf("Time GPU is: %d ms\n", (int) time);
	cudaEventDestroy(start);
	cudaEventDestroy(stop);

    return cudaStatus;
}

void printGpuProperties()
{
	const int kb = 1024;
    const int mb = kb * kb;
    cout << "NBody.GPU" << endl << "=========" << endl << endl;

    int devCount;
    cudaGetDeviceCount(&devCount);
    cout << "CUDA Devices: " << endl << endl;

    for(int i = 0; i < devCount; ++i)
    {
        cudaDeviceProp props;
        cudaGetDeviceProperties(&props, i);
        cout << i << ": " << props.name << ": " << props.major << "." << props.minor << endl;
        cout << "  Global memory:   " << props.totalGlobalMem / mb << "mb" << endl;
        cout << "  Shared memory:   " << props.sharedMemPerBlock / kb << "kb" << endl;
        cout << "  Constant memory: " << props.totalConstMem / kb << "kb" << endl;
        cout << "  Block registers: " << props.regsPerBlock << endl << endl;

        cout << "  Warp size:         " << props.warpSize << endl;
        cout << "  Threads per block: " << props.maxThreadsPerBlock << endl;
        cout << "  Max block dimensions: [ " << props.maxThreadsDim[0] << ", " << props.maxThreadsDim[1]  << ", " << props.maxThreadsDim[2] << " ]" << endl;
        cout << "  Max grid dimensions:  [ " << props.maxGridSize[0] << ", " << props.maxGridSize[1]  << ", " << props.maxGridSize[2] << " ]" << endl;
        cout << endl;
		return;
	}
}