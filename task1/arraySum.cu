#include <iostream>
#include <math.h>
#include <time.h>

using namespace std;

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>

cudaError_t sumArrayWithCuda(unsigned long long *a, unsigned long long *s, int size);
void sumGpu(int size, unsigned long long *a);
void sumCpu(int size, unsigned long long *a);
void printGpuProperties();
int getIterationCount(int size);

__global__ void sumKernel(unsigned long long *a, unsigned long long *s, int n)
{
	extern __shared__ unsigned long long blockSum[];
	int tid = threadIdx.x;
    int i = blockIdx.x * blockDim.x + tid;

	blockSum[tid] = i < n ? a[i] : 0;
	__syncthreads();
	
	for (unsigned int i = 1; i < blockDim.x; i *= 2)
	{
		if (tid % (2 * i) == 0)
		{
			blockSum[tid] += blockSum[tid + i];
		}
    	__syncthreads();
	}
	if (tid == 0)
	{
		s[blockIdx.x] = blockSum[0];
	}
}

const int block_size = 1024;


int main()
{
	int arraySize;
    cout << "Enter array size: ";
    cin >> arraySize;

    unsigned long long *randomArray = new unsigned long long[arraySize]; 

	//��������� ������
	srand(time(NULL));	
    for (int i = 0; i < arraySize; i++) 
    {
        randomArray[i] = rand() % 100;
    }

    sumGpu(arraySize, randomArray);
    sumCpu(arraySize, randomArray);
    
    delete [] randomArray; 
    return 0;
}

void sumCpu(int size, unsigned long long *a)
{
	unsigned long long result = 0;
	unsigned int start_time =  clock(); 
    for (int i = 0; i < size; i++)
    {
        result += a[i];
    }

	unsigned int finish_time =  clock();	
	cout << "Time CPU is: " << finish_time - start_time << " ms" << endl;
	cout << "Sum on CPU = " << result << endl;
}

void sumGpu(int size, unsigned long long *a)
{
    unsigned long long *blockSum = new unsigned long long[size];

	cudaError_t cudaStatus = sumArrayWithCuda(a, blockSum, size);
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "sumArrayWithCuda failed!");
        return;
    }
  
	unsigned long long result = blockSum[0];
	cout << "Sum on GPU = " << result << endl;
  
	delete [] blockSum;
}

cudaError_t sumArrayWithCuda(unsigned long long *a, unsigned long long *s, int size)
{
    unsigned long long *dev_a = 0;
    unsigned long long *dev_s = 0;

    cudaError_t cudaStatus;
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        goto Error;
    }

    //�������� ������
    cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(unsigned long long));
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_s, size * sizeof(unsigned long long));
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    //��������
    cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(unsigned long long), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }
   
	int iterationCount = getIterationCount(size);
    int num_blocks = (size / block_size) + ((size % block_size != 0) ? 1 : 0);	
	size_t shm_size = block_size * sizeof(unsigned long long);

	//�������� �����
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	float time;
	cudaEventRecord(start, 0);
	
	//��������� ������������ ��������
	for (int i = 0; i < iterationCount; i++)
	{
		sumKernel<<<num_blocks, block_size, shm_size>>>(dev_a, dev_s, size);
		dev_a = dev_s;
	}

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);

    //��������� ������
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "sumKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        goto Error;
    }
    
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching sumKernel!\n", cudaStatus);
        goto Error;
    }

    //�������� ���������
	cudaStatus = cudaMemcpy(s, dev_s, size * sizeof(unsigned long long), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) 
    {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

Error:
    cudaFree(dev_a);
    cudaFree(dev_s);

	printf("Time GPU is: %d ms\n", (int) time);
	cudaEventDestroy(start);
	cudaEventDestroy(stop);

    return cudaStatus;
}

int getIterationCount(int size)
{
	int count = 0;
	while (size > 0)
	{
		size /= block_size;
		count++;
	}
	return count;
}

void printGpuProperties()
{
	const int kb = 1024;
    const int mb = kb * kb;
    cout << "NBody.GPU" << endl << "=========" << endl << endl;

    int devCount;
    cudaGetDeviceCount(&devCount);
    cout << "CUDA Devices: " << endl << endl;

    for(int i = 0; i < devCount; ++i)
    {
        cudaDeviceProp props;
        cudaGetDeviceProperties(&props, i);
        cout << i << ": " << props.name << ": " << props.major << "." << props.minor << endl;
        cout << "  Global memory:   " << props.totalGlobalMem / mb << "mb" << endl;
        cout << "  Shared memory:   " << props.sharedMemPerBlock / kb << "kb" << endl;
        cout << "  Constant memory: " << props.totalConstMem / kb << "kb" << endl;
        cout << "  Block registers: " << props.regsPerBlock << endl << endl;

        cout << "  Warp size:         " << props.warpSize << endl;
        cout << "  Threads per block: " << props.maxThreadsPerBlock << endl;
        cout << "  Max block dimensions: [ " << props.maxThreadsDim[0] << ", " << props.maxThreadsDim[1]  << ", " << props.maxThreadsDim[2] << " ]" << endl;
        cout << "  Max grid dimensions:  [ " << props.maxGridSize[0] << ", " << props.maxGridSize[1]  << ", " << props.maxGridSize[2] << " ]" << endl;
        cout << endl;
		return;
	}
}